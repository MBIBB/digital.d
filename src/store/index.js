import Vue from 'vue'
import Vuex from 'vuex'
import user from './modules/user'
import school from './modules/school'
import server from './modules/server'
import notification from './modules/notification'
import ui from './modules/ui'
import getters from './getters'
import VuexPersistence from 'vuex-persist'

Vue.use(Vuex)

const vuexLocal = new VuexPersistence({
  storage: window.localStorage
})

const store = new Vuex.Store({
  modules: {
    user,
    school,
    server,
    notification,
    ui
  },
  getters,
  plugins: [vuexLocal.plugin]
})

export default store
