import service from '@/api/service'

function get (userId) {
  var params = {
    id: `eq.${userId}`
  }

  const config = {
    headers: {
      PREFER: 'return=representation'
    }
  }

  return service.get('/users', { params }, config)
}

function getGroups (userId) {
  const params = {
    user_id: `eq.${userId}`
  }
  const url = '/user_group?select=group_id,idea_space(id,title,description)'
  return service.get(url, { params })
}

function addGroup (group) {
  return service.post('/user_group', group)
}

function removeGroup (userId, groupName) {
  const params = {
    user_id: `eq.${userId}`,
    group_id: `eq.${groupName}`
  }
  return service.delete('/user_group', { params })
}

function create (user) {
  const config = {
    headers: {
      PREFER: 'return=representation'
    }
  }
  return service.post('/rpc/add_user', user, config)
}

function resetPassword (schoolId, userId) {
  const data = {
    school_id: schoolId,
    user_id: userId
  }
  return service.post('/rpc/reset_password', data)
}

function update (user) {
  const config = {
    headers: {
      PREFER: 'return=representation'
    }
  }
  return service.post('/rpc/update_user', user, config)
}

function updateUsername (userId, username) {
  const data = {
    user_id: userId,
    username: username
  }
  return service.post('/rpc/update_userlogin', data)
}

function updateUserConfig (userId, key, value) {
  const data = {
    'user_id': userId,
    'key': key,
    'value': JSON.stringify(value)
  }
  return service.post('/rpc/user_config_update', data)
}

function updatePassword (userId, oldPassword, newPassword) {
  const data = {
    user_id: userId,
    old_password: oldPassword,
    new_password: newPassword
  }
  return service.post('/rpc/change_password', data)
}

function updateEmail (userId, email) {
  return service({
    method: 'patch',
    url: '/users',
    data: { email: email },
    params: {
      id: `eq.${userId}`
    }
  })
}

function updateProfilePicture (userId, userPicture) {
  const params = {
    id: `eq.${userId}`
  }
  return service.patch('/users', { picture: userPicture }, { params })
}

function remove (userId) {
  const params = {
    id: `eq.${userId}`
  }
  return service.delete('/users', { params })
}

function deleteUser (schoolId, userId) {
  const params = {
    schoolid: schoolId,
    userid: userId
  }
  return service.post('/rpc/delete_user', params)
}

function getListing (schoolId = null) {
  const params = {
    schoolid: schoolId
  }
  return service.post('/rpc/user_listing', params)
}

function deleteUserBulk (schoolId, userIds) {
  const params = {
    schoolid: schoolId,
    userids: userIds
  }
  return service.post('/rpc/delete_user_bulk', params)
}

function addRoleBulk (schoolId, userIds, groupId, ideaSpaceId) {
  const params = {
    schoolid: schoolId,
    userids: userIds,
    groupid: groupId,
    ideaspace: ideaSpaceId
  }
  return service.post('/rpc/add_user_roles_bulk', params)
}

function removeRoleBulk (schoolId, userIds, groupId, ideaSpaceId) {
  const params = {
    schoolid: schoolId,
    userids: userIds,
    groupid: groupId,
    ideaspace: ideaSpaceId
  }
  return service.post('/rpc/remove_user_roles_bulk', params)
}

export default {
  get,
  addGroup,
  removeGroup,
  getGroups,
  create,
  update,
  updateUsername,
  updateUserConfig,
  remove,
  getListing,
  updateProfilePicture,
  updateEmail,
  updatePassword,
  resetPassword,
  deleteUser,
  deleteUserBulk,
  addRoleBulk,
  removeRoleBulk
}
