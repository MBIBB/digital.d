import * as Sentry from '@sentry/browser'
import {Vue as VueIntegration} from '@sentry/integrations/dist/vue'
import {Severity} from '@sentry/types/dist/severity'

function gatherDatasForLogging (args) {
  return args.map(a => {
    if (a instanceof Error) {
      return a.toString()
    }
    if ((typeof a).toLowerCase() === 'object') {
      return JSON.stringify(a)
    }
    return a
  }).join(', ')
}

const SentryIntegration = function () {
  let opts = {}
  this.installed = false

  this.configureSentryScope = function (configation) {
    if (!this.installed) { return }
    Sentry.configureScope(function (scope) {
      if ('tags' in configation) {
        scope.setTags(configation.tags)
      }
      if ('extras' in configation) {
        scope.setExtras(configation.extras)
      }
      if ('user' in configation) {
        scope.setUser(configation.user)
      }
    })
  }

  this.sendError = function () {
    if (!this.installed) { return }
    const args = Array.prototype.slice.call(arguments)
    const errorObj = new Error(gatherDatasForLogging(args))
    errorObj.stack = errorObj.stack.split(String.fromCharCode(10)).filter(e => e.indexOf('sentry.integration') === -1).join(String.fromCharCode(10))
    Sentry.captureException(errorObj, {level: Severity.Fatal})
  }

  this.sendInfo = function () {
    if (!this.installed) { return }
    const args = Array.prototype.slice.call(arguments)
    const errorObj = new Error(gatherDatasForLogging(args))
    errorObj.name = 'Info'
    errorObj.stack = errorObj.stack.split(String.fromCharCode(10)).filter(e => e.indexOf('sentry.integration') === -1).join(String.fromCharCode(10))
    Sentry.captureException(errorObj, {level: Severity.Info})
  }

  this.sendWarning = function () {
    if (!this.installed) { return }
    const args = Array.prototype.slice.call(arguments)
    const errorObj = new Error(gatherDatasForLogging(args))
    errorObj.name = 'Warning'
    errorObj.stack = errorObj.stack.split(String.fromCharCode(10)).filter(e => e.indexOf('sentry.integration') === -1).join(String.fromCharCode(10))
    Sentry.captureException(errorObj, {level: Severity.Warning})
  }

  this.captureMessage = function (msg, captureContext) {
    if (!this.installed) { return }
    Sentry.captureMessage(msg, captureContext)
  }

  this.captureException = function (error, captureContext) {
    if (!this.installed) { return }
    Sentry.captureException(error, captureContext)
  }

  this.captureEvent = function (event, captureContext) {
    if (!this.installed) { return }
    Sentry.captureEvent(event, captureContext)
  }

  this.install = function (Vue, options) {
    opts = {
      ...{
        sentryDsn: '',
        disableSentryOnLocalhost: true,
        disableConsoleLogOnProd: true,
        overwriteConsoleMethods: ['error', 'info', 'warn'],
        outputToConsole: false
      },
      ...options
    }

    if (!opts.sentryDsn || opts.sentryDsn.length < 5) {
      console.error('Add the DSN for Sentry!')
      return
    }

    if (location.host.indexOf('localhost') !== -1 && opts.disableSentryOnLocalhost === true) {
      console.info('Sentry will not be used on localhost!')
      return
    }

    console.info('Using Sentry to report Bugs')

    // init sentry
    Sentry.init({
      dsn: opts.sentryDsn,
      environment: process.env.NODE_ENV,
      integrations: [new VueIntegration({Vue, attachProps: true})]
    })

    // deactivate console.log
    if (opts.disableConsoleLogOnProd === true) {
      if (process.env.NODE_ENV === 'Production') {
        console.log = function () {
        }
      }
    }

    // overwrite console methods
    const self = this
    if (opts.overwriteConsoleMethods.indexOf('error') !== -1) {
      window.consoleError = console.error

      console.error = function () {
        const args = Array.prototype.slice.call(arguments)
        self.sendError.apply(self, args)
        if (opts.outputToConsole === true) {
          window.consoleError.apply(console, args)
        }
      }
    }

    if (opts.overwriteConsoleMethods.indexOf('info') !== -1) {
      window.consoleInfo = console.info

      console.info = function () {
        const args = Array.prototype.slice.call(arguments)
        self.sendInfo.apply(self, args)
        if (opts.outputToConsole === true) {
          window.consoleInfo.apply(console, args)
        }
      }
    }

    if (opts.overwriteConsoleMethods.indexOf('warn') !== -1) {
      window.consoleWarn = console.warn

      console.warn = function () {
        const args = Array.prototype.slice.call(arguments)
        self.sendWarning.apply(self, args)
        if (opts.outputToConsole === true) {
          window.consoleWarn.apply(console, args)
        }
      }
    }

    this.installed = true
  }
}

export default new SentryIntegration()
