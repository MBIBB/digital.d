# digital.D

digital.D is an online platform based on aula that allows young people to come up with short ideas, improve on them via peer suggestions, go through a democrativ deliberative process and vote on them online.

Build Setup
# install dependencies
npm install

# serve with hot reload at localhost:8080
npm run dev

# build for production with minification
npm run build

# build for production and view the bundle analyzer report
npm run build --report

# run unit tests
npm run unit

# run e2e tests
npm run e2e

# run all tests
npm test

# build for production and cordova build.
npm run cordova-build

# build for production and serve the app through the browser - no hot reload.
npm run browser

# add respective platforms
cordova platform add android
cordova platform add ios

# build for production and serve the app on an iOS device
npm run ios

# build for production and serve the app on an android device (won't serve on a virtual device)
npm run android

# build for production and serve the app on an android device (will serve on a virtual device or physical device - prefers virtual)
npm run android-vm


## Documentation
Documentation can be found [here](https://delibrium.gitlab.io/aula-app)

## Want to jump in?

 * Bug hunt: Did you find a bug? Please check existing gitlab issues and create a new one if it is not listed yet.
 * Use Cases: How would you like to use aula in school or in your organization? Please describe your idea as a comment to this issue
 * Writing code: Ready to write some javascript with us? These [issues](https://gitlab.com/delibrium/aula-app/issues) are waiting…
 * Discuss: Do you want to see additional features or have feedback about the software? Write us at info@aula.de

## Current Main Features

 * Multiple user groups
    * Multiple rooms
    * Role management: participant, moderator, director, admin, main admin

 * Idea management
    * Create ideas
    * Comment on ideas (make suggestions)
    * Support ideas
    * Group ideas into topics
    * Assign categories

  * Phases
    * Five main phases (wild ideas, discussion, approval, result)
    * Deactivate phases, change phase duration

  * Admin interface
    * Edit and create new rooms
    * Edit categories, create custom categories
    * User management

   * Vote delegation

## Roadmap

Short term - What we are working on now 🎉

* push notifications
* bulk operation on the admin interface
* filtering by categories
* mark ideas as winner
* move topics between rooms
* automated bug reporting
* improved delegation ux

Medium term - what we’re working on next! 🎨

* customizable picture on rooms and topics
* custom fields for ideas creations
* oauth authentication
* reset password by email


Longer term items - working on this soon! ⏩

* custom themes for the interface
* Archive
* search function for delegation
* hide delegation function
