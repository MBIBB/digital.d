module.exports = {
  NODE_ENV: '"production"',
  BASE_API: '"https://app.aula.de/api/"',
  assetsPublicPath: './',
  assetsSubDirectory: './'
}
