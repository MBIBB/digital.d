FROM node:14

VOLUME aula/www/
WORKDIR aula/
COPY package*.json ./
RUN npm install
COPY . .
ENV "ENV" "production"
CMD ["npm", "run", "build"]
